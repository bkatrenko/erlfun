%%%-----------------------------------------------------------------------------
%%% @doc package set using as REST http handler for /set value request, and implement some part
%%% of cowboy REST handler callbacks.
%%% @author Bohdan Katrenko <bohdan@katrenko.me>
%%% @end
%%%-----------------------------------------------------------------------------
-module(set).

-export([init/2, allowed_methods/2, content_types_accepted/2, from_json/2]).

init(Req, Opts) ->
    {cowboy_rest, Req, Opts}.

%%%=============================================================================
%%% Handler Implementation
%%% @doc next functions is just callbacks for cowboy server - and it will can in the same 
%%% order as it implemented here.
%%% @end
%%%=============================================================================

allowed_methods(Req, State)->
    {[<<"POST">>], Req, State}.

content_types_accepted(Req, State)->
	{[
        {<<"application/json">>, from_json}
    ], Req, State}.

from_json(Req, State)->
    {ok, Body, Req2} = cowboy_req:read_body(Req),

    try jiffy:decode(Body, [return_maps]) of
    Decoded ->  {_, Key} = maps:find(<<"key">>, Decoded),
                {_, Value} = maps:find(<<"value">>, Decoded),
                key_value:set(Key, Value),
                {true, Req2, State}
    catch 
          _ ->  {false, Req2, State}
    end.