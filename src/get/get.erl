%%%-----------------------------------------------------------------------------
%%% @doc package get using as REST http handler for /get value request, and implement some part
%%% of cowboy REST handler callbacks.
%%% @author Bohdan Katrenko <bohdan@katrenko.me>
%%% @end
%%%-----------------------------------------------------------------------------
-module(get).

-export([init/2, allowed_methods/2, resource_exists/2, content_types_provided/2, to_json/2]).

init(Req, Opts) ->
    {cowboy_rest, Req, Opts}.

%%%=============================================================================
%%% Handler Implementation
%%% @doc next functions is just callbacks for cowboy server - and it will can in the same 
%%% order as it implemented here.
%%% @end
%%%=============================================================================

allowed_methods(Req, State)->
    {[<<"GET">>], Req, State}.

resource_exists(Req, State)->
    {_, Key} = maps:find(key, cowboy_req:match_qs([{key, [], <<"">>}], Req)),
    case key_value:get(Key) of 
        {ok, Value} -> 
            {true, Req, Value};
        error -> 
            {false, Req, State}
    end.

content_types_provided(Req, State)->
	{[
        {<<"application/json">>, to_json}
    ], Req, State}.

to_json(Req, State) ->
    {jiffy:encode(State), Req, State}.