%%%-------------------------------------------------------------------
%% @doc play top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(play_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

init([]) ->
    SupervisorSpecification = #{
        strategy => one_for_one,
        intensity => 10,
        period => 60},

        ChildSpecifications =
        %% here is cowboy supervisor specification. 
        %% The supervisor is responsible for starting, stopping, and monitoring its child processes. 
        %% The basic idea of a supervisor is that it must keep its child processes alive 
        %% by restarting them when necessary.
        [#{id => http, %% here is http server starting
           start => {http, init, [2938]},
           restart => permanent,
           shutdown => 2000,
           type => worker,
           modules => [http]},
        %% here is starting gen_server wiht key_value storage.
        %% Gen server is smth like base client-server app with internal state to store data and 
        %% very basic part of OTP
         #{id => key_value,
           start => {key_value, start, []},
           restart => permanent,
           shutdown => 2000,
           type => worker,
           modules => [hey_value]}
        ],

        %% you can read more about supervisor here: http://erlang.org/doc/man/supervisor.html
    {ok, {SupervisorSpecification, ChildSpecifications}}.