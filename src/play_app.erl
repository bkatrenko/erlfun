%%%-------------------------------------------------------------------
%% @doc play public API
%% @end
%%%-------------------------------------------------------------------

-module(play_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%%====================================================================
%% API
%%====================================================================

%% here we just start supervisor that do all another work :)
start(_StartType, _StartArgs) ->
   'play_sup':start_link().

%%--------------------------------------------------------------------
stop(_State) ->
    ok.