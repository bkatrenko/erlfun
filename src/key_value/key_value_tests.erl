-module(key_value_tests).

-include_lib("eunit/include/eunit.hrl").

set_test()->
    Key = "key",
    Value = "value",

    key_value:start(),
    key_value:set(Key, Value),
    {_, Res} = key_value:get(Key),
    
    ?assertEqual(Value, Res).

get_test()->
    Key = "key",
    Value = "value",
    BadKey = "bad_key",

    key_value:start(),
    key_value:set(Key, Value),
    {_, Res} = key_value:get(Key),

    ?assertEqual(Value, Res),

    Res2 = key_value:get(BadKey),
    ?assertEqual(error, Res2).