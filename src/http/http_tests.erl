-module(http_tests).

-include_lib("eunit/include/eunit.hrl").

start_test()->
    start_server().

set_test()->
    start_server(),
    Request = {"http://localhost:9091/set", [], "application/json", <<"{\"key\":\"lalala\", \"value\":\"ohoho\"}">>},
    {ok, {{_, 204, "No Content"}, _, _}} = httpc:request(post, Request, [], []),

    {ok,{{_,200,"OK"}, _, Res}} = httpc:request(get, {"http://localhost:9091/get?key=lalala", []}, [], []),
    ?assertEqual("\"ohoho\"", Res). 

get_not_found_test()-> 
    start_server(),
    {ok,{{_,404, "Not Found"}, _, _}} = httpc:request(get, {"http://localhost:9091/get?key=not_found_key", []}, [], []).

start_server() ->
    application:ensure_all_started(cowboy),
    http:init(9091),
    key_value:start(),
    inets:start().