%%%-----------------------------------------------------------------------------
%%% @doc package http provide http server implementation with cowboy framework.
%%% @author Bohdan Katrenko <bohdan@katrenko.me>
%%% @end
%%%-----------------------------------------------------------------------------
-module(http).

-export([init/1]).

init(Port)->
    Dispatch = cowboy_router:compile([
		%% routes definition here
		{'_', [
			{"/get", get, []},
            {"/set", set, []}
		]}
	]),

	cowboy:start_clear(http, [{port, Port}], #{
		env => #{dispatch => Dispatch
	}}).
