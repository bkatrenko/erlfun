FROM erlang:latest

# install Rebar3
RUN mkdir -p /buildroot/rebar3/bin
ADD https://s3.amazonaws.com/rebar3/rebar3 /buildroot/rebar3/bin/rebar3
RUN chmod a+x /buildroot/rebar3/bin/rebar3

# setup Environment
ENV PATH=/buildroot/rebar3/bin:$PATH

# reset working directory
WORKDIR /buildroot

# copy app to container
COPY . /buildroot/play

# build the release
WORKDIR /buildroot/play
RUN rebar3 release

# expose app port
EXPOSE 2938

# rrooooo, run 'em all!
CMD ["_build/default/rel/play_release/bin/play_release", "foreground"]