play
=====

An OTP application as simple key/value json storage built with cowboy erlang web framework (https://ninenines.eu).
It's don't very production ready, but can be useful as base for another apps and as education material.

Build
-----

    This app built with rebar3 manager

To compile:
-----

    $ rebar3 compile

To test:
-----

    $ run unit tests with [rebar3 eunit]
    $ for manual testing you can use test.http file in root directory
    $ for load testing go to vegeta dir and exec [echo | vegeta attack -duration=5s -rate=5000/1s -targets=targets | vegeta report]

To try it:
-----

    $ start with [rebar3 shell] or ->
    $ build a release with [rebar3 release]
    $ start release with [./_build/default/rel/play_release/bin/play_release <console>|<start>]

If you'd like docker: 
-----

    $ [docker build -t erlang-play .] -> [docker run -d -p=2938:2938 -d  erlang-play]

I wish you nice playing with Erlang (@_@)    
